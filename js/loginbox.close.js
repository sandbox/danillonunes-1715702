/**
 * Append close button to Login-box element.
 */
Drupal.behaviors.loginboxClose = function(context) {
  var $loginbox = $('#loginbox', context),
      $close = $('<button class="loginbox-close" title="' + Drupal.t('Close') + '">' + Drupal.t('Close') + '</button>');

  $close
    .prependTo($loginbox)
    .bind('click.loginbox:close', function() {
      $loginbox.trigger('loginbox:close');
    });
};
