
/**
 * Copy background styles from #wrapper.
 *
 * This allows Login-box to have the same look'n'feel from the site even when
 * its customized with Color module.
 */
Drupal.behaviors.loginboxGarland = function(context) {
  var $wrapper = $('#wrapper');

  $('#loginbox', context).css({
    'background-color': $wrapper.css('background-color'),
    'background-image': $wrapper.css('background-image')
  });
};
